import numpy as np
import math as m
import matplotlib.pyplot as plt
from scipy.integrate import quad
import scipy as sc
from scipy.integrate import romberg

# --------------------
# Result generator for PS
# --------------------


def get_exact_PS(theta):
    exact_x = [-0.5*p0/m.pi*(2*(theta_[0]-theta_[1])+(m.sin(2*theta_[0])-m.sin(2*theta_[1]))) for theta_ in theta]
    exact_z = [-0.5*p0/m.pi*(2*(theta_[0]-theta_[1])-(m.sin(2*theta_[0])-m.sin(2*theta_[1]))) for theta_ in theta]
    exact_xz = [-0.5*p0/m.pi*(m.cos(2*theta_[0])-m.cos(2*theta_[1])) for theta_ in theta]
    exact_y = [nu*(exact_x_i + exact_z_i) for exact_x_i, exact_z_i in zip(exact_x, exact_z)]
    return exact_x, exact_z, exact_xz, exact_y


def get_exact_AXI(rss, zss):
    sigma_r = []
    sigma_z = []
    sigma_rz = []
    sigma_theta = []
    for i in range(len(rss)):
        sigma_r.append(get_AS_exact_NI_sr(rss[i], zss[i]))
        sigma_z.append(get_AS_exact_NI_sz(rss[i], zss[i]))
        sigma_rz.append(get_AS_exact_NI_srz(rss[i], zss[i]))
        sigma_theta.append(get_AS_exact_NI_stheta(rss[i], zss[i]))
    return sigma_r, sigma_z, sigma_rz, sigma_theta


def get_exact_3D(rss, zss):
    sigma_x = []
    sigma_y = []
    sigma_z = []
    sigma_xy = []
    sigma_xz = []
    sigma_yz = []
    for i in range(len(rss)):
        sigma_x.append(sigma_x_3D(rss[i], 0.0, zss[i]))
        sigma_y.append(sigma_y_3D(rss[i], 0.0, zss[i]))
        sigma_z.append(sigma_z_3D(rss[i], 0.0,  zss[i]))
        sigma_xy.append(sigma_xy_3D(rss[i], 0.0, zss[i]))
        sigma_xz.append(sigma_zx_3D(rss[i], 0.0, zss[i]))
        sigma_yz.append(sigma_yz_3D(rss[i], 0.0, zss[i]))
    return sigma_x, sigma_y, sigma_z, sigma_xy, sigma_xz, sigma_yz


# def get_one_exact(theta):
#     exact_x = -0.5*p0/math.pi*(2*(theta[0]-theta[1])+(math.sin(2*theta[0])-math.sin(2*theta[1])))
#     exact_y = -0.5*p0/math.pi*(2*(theta[0]-theta[1])-(math.sin(2*theta[0])-math.sin(2*theta[1])))
#     exact_xy = -0.5*p0/math.pi*(math.cos(2*theta[0])-math.cos(2*theta[1]))
#     exact_z = nu*(exact_x + exact_y)
#     return exact_x, exact_y, exact_xy, exact_z


# Integrand for sigma_r, first part
def integrand_bf_sr_1(s, r, z):
    return -(1-s*z)*p0*a*sc.special.j1(a*s)*sc.special.j0(r*s)*m.exp(-s*z)


# Integrand for sigma_z
def integrand_bf_sz(s, r, z):
    return -(1+s*z)*m.exp(-s*z)*p0*a*sc.special.j1(a*s)*sc.special.j0(r*s)


# Integrand for sigma_r, second part
def integrand_bf_sr_2(s, r, y):
    return 1/r*((1-2*nu)/s-y)*p0*a*sc.special.j1(a*s)*sc.special.j1(r*s)*m.exp(-s*y)


# Integrand for sigma_rz
def integrand_bf_srz(s, r, z):
    return -s*z*p0*a*sc.special.j1(a*s)*sc.special.j1(r*s)*m.exp(-s*z)


# Integrand for sigma_theta, first part
def integrand_bf_stheta_1(s, r, z):
    return -2*nu*p0*a*sc.special.j1(a*s)*sc.special.j0(r*s)*np.exp(-s*z)


# Integrand for sigma_theta, second part
def integrand_bf_stheta_2(s, r, z):
    return -1/r*((1-2*nu)/s-0.5*z)*p0*a*sc.special.j1(a*s)*sc.special.j1(r*s)*np.exp(-s*z)


# numerical integration of sigma_z
def get_AS_exact_NI_sz(r, z):
    intVal = romberg(integrand_bf_sz, 1e-10, 100.0, args=(r, z), rtol=1e-5, tol=1e-14, divmax=40)
    #return quad(integrand_bf_sz, 1e-10, 50.0, args=(r, z))[0]
    return intVal


# numerical integration of sigma_r
def get_AS_exact_NI_sr(r, z):
    return quad(integrand_bf_sr_1, 0, np.inf, args=(r, z))[0] + quad(integrand_bf_sr_2, 0, np.inf, args=(r, z))[0]


# numerical integration of sigma_rz
def get_AS_exact_NI_srz(r, z):
    return quad(integrand_bf_srz, 0, np.inf, args=(r, z))[0]


# numerical integration of sigma_theta
def get_AS_exact_NI_stheta(r, z):
    intVal = romberg(integrand_bf_stheta_1, 1e-10, 500.0, args=(r, z),rtol=1e-5,tol=1e-14,divmax=40)
    intVal += romberg(integrand_bf_stheta_2, 1e-10, 500.0, args=(r, z), rtol=1e-5, tol=1e-14, divmax=40)
    return intVal


def sigma_z_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    dVdz_i = dVdz(x, y, z)
    d2Vdz2_i = d2Vdz2(x, y, z, a1, b2, c3, d4)
    temp = 0.5/m.pi*(dVdz_i - z*d2Vdz2_i)
    return temp


def sigma_x_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    dVdz_i = dVdz(x, y, z)
    d2chidx2_i = d2chidx2(x, y, z, a1, b2, c3, d4)
    d2Vdx2_i = d2Vdx2(x, y, z, a1, b2, c3, d4)
    temp = 0.5/m.pi*(2*nu*dVdz_i - (1-2*nu)*d2chidx2_i - z*d2Vdx2_i)
    return temp


def sigma_y_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    dVdz_i = dVdz(x, y, z)
    d2chidy2_i = d2chidy2(x, y, z, a1, b2, c3, d4)
    d2Vdy2_i = d2Vdy2(x, y, z, a1, b2, c3, d4)
    temp = 0.5/m.pi*(2*nu*dVdz_i - (1-2*nu)*d2chidy2_i - z*d2Vdy2_i)
    return temp


def sigma_yz_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    d2Vdydz_i = d2Vdydz(x, y, z, a1, b2, c3, d4)
    temp = -0.5/m.pi*z*d2Vdydz_i
    return temp


def sigma_zx_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    d2Vdzdx_i = d2Vdzdx(x, y, z, a1, b2, c3, d4)
    temp = -0.5/m.pi*z*d2Vdzdx_i
    return temp


def sigma_xy_3D(x, y, z):
    a1, b2, c3, d4 = calc_dist_corners(x, y, z)
    d2chidxdy_i = d2chidxdy(x, y, z, a1, b2, c3, d4)
    d2Vdxdy_i = d2Vdxdy(x, y, z, a1, b2, c3, d4)
    temp = -0.5/m.pi*((1-2*nu)*d2chidxdy_i + z*d2Vdxdy_i)
    return temp


def calc_dist_corners(x, y, z):
    a1 = m.sqrt((x - a)**2 + (y - a)**2 + z**2)
    b2 = m.sqrt((x + a)**2 + (y - a)**2 + z**2)
    c3 = m.sqrt((x + a)**2 + (y + a)**2 + z**2)
    d4 = m.sqrt((x - a)**2 + (y + a)**2 + z**2)
    return a1, b2, c3, d4


def dVdz(x, y, z):
    temp = 2*m.pi
    temp -= m.acos((a-x)*(a-y)/(m.sqrt((a-x)**2 + z**2)*m.sqrt((a-y)**2 + z**2)))
    temp -= m.acos((a-x)*(a+y)/(m.sqrt((a-x)**2 + z**2)*m.sqrt((a+y)**2 + z**2)))
    temp -= m.acos((a+x)*(a-y)/(m.sqrt((a+x)**2 + z**2)*m.sqrt((a-y)**2 + z**2)))
    temp -= m.acos((a+x)*(a+y)/(m.sqrt((a+x)**2 + z**2)*m.sqrt((a+y)**2 + z**2)))
    return -p0*temp


def d2Vdz2(x, y, z, a1, b2, c3, d4):
    temp = (a-x)/((a-x)**2 + z**2)*((a-y)/a1 + (a+y)/d4)
    temp += (a+x)/((a+x)**2 + z**2)*((a-y)/b2 + (a+y)/c3)
    temp += (a-y)/((a-y)**2 + z**2)*((a-x)/a1 + (a+x)/b2)
    temp += (a+y)/((a+y)**2 + z**2)*((a-x)/d4 + (a+x)/c3)
    return p0*temp


def d2Vdy2(x, y, z, a1, b2, c3, d4):
    temp = (a-y)/((a-y)**2 + z**2)*((a-x)/a1 + (a+x)/b2)
    temp += (a+y)/((a+y)**2 + z**2)*((a-x)/d4 + (a+x)/c3)
    return -p0*temp


def d2Vdx2(x, y, z, a1, b2, c3, d4):
    temp = (a-x)/((a-x)**2 + z**2)*((a-y)/a1 + (a+y)/d4)
    temp += (a+x)/((a+x)**2 + z**2)*((a-y)/b2 + (a+y)/c3)
    return -p0*temp


def d2Vdydz(x, y, z, a1, b2, c3, d4):
    temp = z/((a-y)**2 + z**2)*((a-x)/a1 + (a+x)/b2)
    temp -= z/((a+y)**2 + z**2)*((a-x)/d4 + (a+x)/c3)
    return p0*temp


def d2Vdzdx(x, y, z, a1, b2, c3, d4):
    temp = z/((a-x)**2 + z**2)*((a-y)/a1 + (a+y)/d4)
    temp -= z/((a+x)**2 + z**2)*((a-y)/b2 + (a+y)/c3)
    return p0*temp


def d2Vdxdy(x, y, z, a1, b2, c3, d4):
    temp = 1.0/a1 - 1.0/b2 + 1.0/c3 - 1.0/d4
    return p0*temp


def d2chidx2(x, y, z, a1, b2, c3, d4):
    temp = m.atan2(a-y, a-x) + m.atan2(a+y, a-x) - m.atan2(z*(a-y), (a-x)*a1) - m.atan2(z*(a+y), (a-x)*d4)
    temp += m.atan2(a-y, a+x) + m.atan2(a+y, a+x) - m.atan2(z*(a-y), (a+x)*b2) - m.atan2(z*(a+y), (a+x)*c3)
    return p0*temp


def d2chidy2(x, y, z, a1, b2, c3, d4):
    temp = m.atan2(a-x, a-y) + m.atan2(a+x, a-y) - m.atan2(z*(a-x), (a-y)*a1) - m.atan2(z*(a+x), (a-y)*b2)
    temp += m.atan2(a-x, a+y) + m.atan2(a+x, a+y) - m.atan2(z*(a-x), (a+y)*d4) - m.atan2(z*(a+x), (a+y)*c3)
    return p0*temp


def d2chidxdy(x, y, z, a1, b2, c3, d4):
    temp = m.log(((z+a1)*(z + c3))/((z+b2)*(z+d4)))
    return p0*temp


def savelinePS(file, xsi, zsi):
    theta = []
    for i in range(len(xsi)):
        theta.append((m.atan2(zsi[i], xsi[i]-a), m.atan2(zsi[i], xsi[i]+a)))
    sigma_x, sigma_z, sigma_xz, sigma_y = get_exact_PS(theta)
    np.savetxt(file, np.column_stack((xsi, zsi, sigma_x, sigma_z, sigma_xz, sigma_y)),
               header="x\tz\tsigma_xx\tsigma_zz\t sigma_xz\t sigma_yy")


def savelineAXI(file, rsi, zsi):
    sigma_x, sigma_z, sigma_xz, sigma_y = get_exact_AXI(rsi, zsi)
    plt.plot(rsi, sigma_z)
    plt.show()
    np.savetxt(file, np.column_stack((rsi, zsi, sigma_x, sigma_z, sigma_xz, sigma_y)),
               header="r\tz\tsigma_rr\tsigma_zz\t sigma_rz\t sigma_theta")


def saveline3D(file, xsi, zsi):
    sigma_x, sigma_y, sigma_z, sigma_xy, sigma_xz, sigma_yz = get_exact_3D(xsi, zsi)
    np.savetxt(file, np.column_stack((xsi, zsi, sigma_x, sigma_y, sigma_z, sigma_xy, sigma_xz, sigma_yz)),
               header="x\tz\tsigma_xx\tsigma_yy\t sigma_zz\t sigma_xy\t sigma_xz\t sigma_yz")


# Parameters
a = 1.0
p0 = 1.0
nu = 0.3

# A line - PS
epsilon = 0.05283
zs = np.linspace(0.0, 5.0, 1000)
xs = epsilon*np.ones(len(zs))
#savelinePS("Results/Benchmark_analytic_PS_Aline.txt", xs, zs)  # Plane strain

# B line - PS
#savelinePS("Results/Benchmark_analytic_PS_Bline.txt", zs, xs)  # Plane strain

# A line - AXI
epsilon = 0.10566
zs = np.linspace(epsilon, 5.0, 1000)
xs = epsilon*np.ones(len(zs))
savelineAXI("Results/Benchmark_analytic_AXI_Aline.txt", xs, zs)  # Axisymmetry

# B line - AXI
#savelineAXI("Results/Benchmark_analytic_AXI_Bline.txt", zs, xs)  # Axisymmetry

# A line - 3D
epsilon = 0.10566  # ?
zs = np.linspace(0.0, 5.0, 1000)
xs = epsilon*np.ones(len(zs))
# saveline3D("Results/Benchmark_analytic_3D_Aline.txt", xs, zs)  # 3D

# B line - 3D
# saveline3D("Results/Benchmark_analytic_3D_Bline.txt", zs, xs)  # 3D
