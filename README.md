## Semi-space benchmark - Support files

This git repository provides support for article "Definition of a challenging benchmark test fornumerical solution of elasto-static problems".

The python script generates data sets for analytical solutions. It can be run by
```
python3 result_generator.py
```
